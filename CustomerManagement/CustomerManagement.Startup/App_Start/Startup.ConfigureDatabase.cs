﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using CustomerManagement.Domain;
using CustomerManagement.Repository;
using Microsoft.Extensions.Configuration;

namespace Customer.Web
{
    public partial class Startup
    {
        private void ConfigureDatabase(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<CustomerManagementDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("Context")));
        }
    }
}
