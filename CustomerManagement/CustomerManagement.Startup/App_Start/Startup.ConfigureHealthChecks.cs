﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;

namespace Customer.Web
{
    public partial class Startup
    {
        private void ConfigureHealthChecks(IServiceCollection services)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddCheck("self", () => HealthCheckResult.Healthy());

            hcBuilder
                .AddSqlServer(
                    Configuration.GetConnectionString("Context"),
                    name: "db-check",
                    tags: new string[] { "customerManagementdb" });
        }
    }
}
