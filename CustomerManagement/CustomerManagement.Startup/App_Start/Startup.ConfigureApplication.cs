﻿using CustomerManagement.Application.Behaviours;
using CustomerManagement.Application.Create;
using CustomerManagement.Application.Queries.GetCustomer;
using CustomerManagement.Startup.Configuration;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using AutoMapper;
using CustomerManagement.Application.Mappings;

namespace Customer.Web
{
    public partial class Startup
    {
        private void ConfigureApplication(IServiceCollection services)
        {
            services
                 .Configure<ApplicationSettings>(
                    Configuration.GetSection(nameof(ApplicationSettings)),
                    options => options.BindNonPublicProperties = true)
                .AddAutoMapper(typeof(MappingProfile))
                .AddMediatR(Assembly.GetAssembly(typeof(CreateCustomerCommandHandler)))
                .AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
        }
    }
}
