﻿using CustomerManagement.Application.Behaviours;
using CustomerManagement.Application.Customers.Create;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Customer.Web
{
    public partial class Startup
    {
        public void ConfigureWeb(IServiceCollection services)
        {
            services.AddMvcCore()
                  .AddApiExplorer()
                  .AddFluentValidation(s =>
                  {
                      s.RegisterValidatorsFromAssemblyContaining<CreateCustomerCommandValidator>();
                  });

            services.AddControllers();

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }
    }
}
