﻿CREATE TABLE [dbo].[Address]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AddressLine1] NVARCHAR(80) NOT NULL, 
    [AddressLine2] NVARCHAR(80) NULL, 
    [Town] NVARCHAR(50) NOT NULL, 
    [County] NVARCHAR(50) NULL, 
    [Postcode] NVARCHAR(10) NOT NULL, 
    [Country] NVARCHAR(20) NULL DEFAULT 'UK',
    [CustomerId] INT NOT NULL,
    [IsMainAddress] BIT NOT NULL DEFAULT 0

)
