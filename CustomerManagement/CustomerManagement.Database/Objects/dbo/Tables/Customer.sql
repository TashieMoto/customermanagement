﻿CREATE TABLE [dbo].[Customer]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(20) NOT NULL, 
    [Forename] NVARCHAR(50) NOT NULL, 
    [Surname] NVARCHAR(50) NOT NULL, 
    [EmailAddress] NVARCHAR(75) NOT NULL, 
    [MobileNo] NVARCHAR(15) NOT NULL, 
    [IsActive] BIT NOT NULL DEFAULT 1
)
