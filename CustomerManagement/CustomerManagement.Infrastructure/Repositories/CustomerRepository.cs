﻿using CustomerManagement.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerManagement.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {

        public CustomerRepository(CustomerManagementDbContext context) : base(context)
        {

        }

        public async Task<Customer> GetCustomerDetails(int id)
        {
            return await _context.Customers
                .Include(x => x.Addresses)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Customer>> GetAllCustomerDetails()
        {
            return await _context.Customers
                .Include(x => x.Addresses)
                .ToListAsync();
        }

        public void RemoveAddress(Address address)
        {
            _context.Set<Address>().Remove(address);
        }

        public async Task<IEnumerable<Customer>> GetAllActiveCustomerDetails()
        {
            return await _context.Customers
                .Include(x => x.Addresses)
                .Where(x => x.IsActive)
                .ToListAsync();
        }

        public async Task<Customer> FindAsync(string title, string forename, string surname, string emailAddress, string mobileNo)
        {
            return await _context.Customers
                .Where(b => b.Title == title && b.Forename == forename && b.Surname == surname && b.EmailAddress == emailAddress && b.MobileNo == mobileNo)
                .SingleOrDefaultAsync();
        }
    }
}
