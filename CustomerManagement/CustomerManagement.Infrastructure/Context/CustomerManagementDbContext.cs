﻿using CustomerManagement.Domain.Models;
using CustomerManagement.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagement.Repository
{
    public class CustomerManagementDbContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "dbo";

        public CustomerManagementDbContext(DbContextOptions<CustomerManagementDbContext> options) : base(options)
        { }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Address> Address { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new AddressConfiguration());
        }

    }
}
