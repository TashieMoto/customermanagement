﻿using CustomerManagement.Domain.Models;
using CustomerManagement.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Infrastructure.EntityConfigurations
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> addressConfiguration)
        {
            addressConfiguration.ToTable("Address", CustomerManagementDbContext.DEFAULT_SCHEMA);

            addressConfiguration.HasKey(o => o.Id);

            addressConfiguration.Property<int>("CustomerId")
                .IsRequired();
        }
    }

}
