﻿using CustomerManagement.Domain.Models;
using CustomerManagement.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CustomerManagement.Infrastructure.EntityConfigurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> customerConfiguration)
        {
            customerConfiguration.ToTable("Customer", CustomerManagementDbContext.DEFAULT_SCHEMA);

            customerConfiguration.HasKey(o => o.Id);

            customerConfiguration.HasMany(b => b.Addresses)
                   .WithOne()
                   .HasForeignKey("CustomerId")
                   .OnDelete(DeleteBehavior.Cascade);

            var navigation = customerConfiguration.Metadata.FindNavigation(nameof(Customer.Addresses));
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
