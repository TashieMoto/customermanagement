﻿using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CustomerManagementDbContext _context;

        private ICustomerRepository _customerRepository;

        public UnitOfWork(CustomerManagementDbContext customerManagementDbContext)
        {
            _context = customerManagementDbContext;
        }

        public ICustomerRepository CustomerRepository => _customerRepository ??= new CustomerRepository(_context);

        public async Task<bool> Commit(CancellationToken cancellationToken = new CancellationToken())
        {
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
