﻿using CustomerManagement.Application.Commands.Delete;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test.Commands
{
    [TestFixture]
    public class DeleteCustomerCommandHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private DeleteCustomerCommandHandler _deleteCustomerCommandHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _deleteCustomerCommandHandler = new DeleteCustomerCommandHandler(_unitOfWork.Object);
        }

        [Test]
        public void Handle_CustomerDoesNotExist_NotFoundExceptionIsThrown()
        {
            var command = new DeleteCustomerCommand
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.Get(command.Id)).ReturnsAsync((Customer)null);

            Assert.ThrowsAsync<NotFoundException>(() => _deleteCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task Handle_CustomerIFound_CustomerIsDeletedAsync()
        {
            var command = new DeleteCustomerCommand
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.Get(command.Id)).ReturnsAsync(new Customer("test", "test", "test", "test", "test"));
            _unitOfWork.Setup(x => x.Commit(default(CancellationToken))).Returns(Task.FromResult(true));

            var result = await _deleteCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.IsTrue(result);
            _unitOfWork.Verify(x => x.CustomerRepository.Delete(It.IsAny<Customer>()));
        }
    }
}
