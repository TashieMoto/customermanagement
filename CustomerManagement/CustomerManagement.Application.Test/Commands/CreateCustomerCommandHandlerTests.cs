﻿using CustomerManagement.Application.Create;
using CustomerManagement.Application.Customers.Create;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using static CustomerManagement.Application.Customers.Create.CreateCustomerCommand;

namespace CustomerManagement.Application.Test.Commands
{
    [TestFixture]
    public class CreateCustomerCommandHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private CreateCustomerCommandHandler _createCustomerCommandHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _createCustomerCommandHandler = new CreateCustomerCommandHandler(_unitOfWork.Object);
        }

        [Test]
        public void Handle_CustomerAlreadyExists_CustomerAlreadyExistsExceptionIsThrown()
        {
            var command = new CreateCustomerCommand { 
                Title = "test",
                Forename = "test",
                Surname = "test",
                EmailAddress = "33@g.co.uk"
            };
            _unitOfWork.Setup(x => x.CustomerRepository.FindAsync(command.Title, command.Forename, command.Surname, command.EmailAddress, command.MobileNo)).ReturnsAsync(new Domain.Models.Customer(command.Title, command.Forename, command.Surname, command.EmailAddress, command.MobileNo));

            Assert.ThrowsAsync<CustomerAlreadyExistsException>(() => _createCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task Handle_CustomerIsNotFound_NewCustomerIsSavedAsync()
        {
            var command = new CreateCustomerCommand
            {
                Title = "test",
                Forename = "test",
                Surname = "test",
                EmailAddress = "33@g.co.uk",
                MainAddress = new CreateCustomerCommand.AddressDTO
                {
                    AddressLine1 = "test",
                    AddressLine2 = "test",
                    Town = "dd",
                    Country = "tet",
                    County = "ee",
                    Postcode = "fwefw"
                },
                Addresses = new List<AddressDTO>()         
            };
            _unitOfWork.Setup(x => x.CustomerRepository.FindAsync(command.Title, command.Forename, command.Surname, command.EmailAddress, command.MobileNo)).ReturnsAsync((Customer) null);
            _unitOfWork.Setup(x => x.Commit(default(CancellationToken))).Returns(Task.FromResult(true));

            var result = await _createCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.IsTrue(result);
            _unitOfWork.Verify(x => x.CustomerRepository.Add(It.IsAny<Customer>()));
        }

    }
}
