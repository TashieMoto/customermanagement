﻿using CustomerManagement.Application.Commands.DeactivateCustomer;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test.Commands
{
    [TestFixture]
    public class DeactivateCustomerCommandHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private DeactivateCustomerCommandHandler _deactivateCustomerCommandHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _deactivateCustomerCommandHandler = new DeactivateCustomerCommandHandler(_unitOfWork.Object);
        }

        [Test]
        public void Handle_CustomerDoesNotExist_NotFoundExceptionIsThrown()
        {
            var command = new DeactivateCustomerCommand
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.Get(command.Id)).ReturnsAsync((Customer) null);

            Assert.ThrowsAsync<NotFoundException>(() => _deactivateCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task Handle_CustomerIFound_CustomerIsDeactivatedAsync()
        {
            var command = new DeactivateCustomerCommand
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.Get(command.Id)).ReturnsAsync(new Customer("test", "test", "test", "test", "test"));
            _unitOfWork.Setup(x => x.Commit(default(CancellationToken))).Returns(Task.FromResult(true));

            var result = await _deactivateCustomerCommandHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.IsTrue(result);
            _unitOfWork.Verify(x => x.CustomerRepository.Update(It.IsAny<Customer>()));
        }
    }
}
