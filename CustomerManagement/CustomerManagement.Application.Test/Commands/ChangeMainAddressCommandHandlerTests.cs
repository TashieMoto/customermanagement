using CustomerManagement.Application.Commands.ChangeMainAddress;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test
{
    [TestFixture]
    public class ChangeMainAddressCommandHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private ChangeMainAddressCommandHandler _changeMainAddressCommandHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _changeMainAddressCommandHandler = new ChangeMainAddressCommandHandler(_unitOfWork.Object);
        }

        [Test]
        public void Handle_CustomerIsNotFound_NotFoundExceptionIsThrown()
        {
            var command = new ChangeMainAddressCommand { AddressId = 1, Id = 1 };
            _unitOfWork.Setup(x => x.CustomerRepository.GetCustomerDetails(command.Id)).ReturnsAsync((Customer)null);

            Assert.ThrowsAsync<NotFoundException>(() => _changeMainAddressCommandHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public void Handle_AddressIdIsNotFoundOnCustomer_NotFoundExceptionIsThrown()
        {
            var command = new ChangeMainAddressCommand { AddressId = 1, Id = 0 };
            var customer = new Customer("test", "test", "test", "test", "test");
            customer.Addresses.Add(new Address("test", "test", "test", "test", "test", "test", false));
            _unitOfWork.Setup(x => x.CustomerRepository.GetCustomerDetails(command.Id)).ReturnsAsync(customer);

            Assert.ThrowsAsync<NotFoundException>(() => _changeMainAddressCommandHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task Handle_CustomerAndAddressAreFound_MainAddressIsChangedAsync()
        {
            var command = new ChangeMainAddressCommand { AddressId = 0, Id = 0 };
            var customer = new Customer("test", "test", "test", "test", "test");
            customer.Addresses.Add(new Address("test", "test", "test", "test", "test", "test", false));
            _unitOfWork.Setup(x => x.CustomerRepository.GetCustomerDetails(command.Id))
                .ReturnsAsync(customer);
            _unitOfWork.Setup(x => x.Commit(default(CancellationToken))).Returns(Task.FromResult(true));

            var result = await _changeMainAddressCommandHandler.Handle(command, new System.Threading.CancellationToken());

            Assert.IsTrue(result);
            _unitOfWork.Verify(x => x.CustomerRepository.Update(It.IsAny<Customer>()));
        }
    }
}
