﻿using CustomerManagement.Application.Commands.Create;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation.TestHelper;
using static CustomerManagement.Application.Customers.Create.CreateCustomerCommand;

namespace CustomerManagement.Application.Test.Validation
{
    [TestFixture]
    public class AddressDTOValidatorTests
    {
        private AddressDTOValidator _addressValidator;

        [SetUp]
        public void SetUp()
        {
            _addressValidator = new AddressDTOValidator();
        }

        [Test]
        public void AddressDTO_AddressLine1IsNull_ShouldHaveAnError()
        {
            var model = new AddressDTO { AddressLine1 = null };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.AddressLine1);
        }

        [Test]
        public void AddressDTO_AddressLine1IsEmpty_ShouldHaveAnError()
        {
            var model = new AddressDTO { AddressLine1 = string.Empty };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.AddressLine1);
        }

        [Test]
        public void AddressDTO_AddressLine1IsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { AddressLine1 = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.AddressLine1);
        }

        [Test]
        public void AddressDTO_AddressLine2IsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { AddressLine2 = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.AddressLine2);
        }

        [Test]
        public void AddressDTO_TownIsNull_ShouldHaveAnError()
        {
            var model = new AddressDTO { Town = null };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Town);
        }

        [Test]
        public void AddressDTO_TownIsEmpty_ShouldHaveAnError()
        {
            var model = new AddressDTO { Town = string.Empty };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Town);
        }

        [Test]
        public void AddressDTO_TownIsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { Town = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Town);
        }

        [Test]
        public void AddressDTO_CountyIsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { County = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.County);
        }

        [Test]
        public void AddressDTO_CountryIsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { Country = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Country);
        }

        [Test]
        public void AddressDTO_PostcodeIsNull_ShouldHaveAnError()
        {
            var model = new AddressDTO { Postcode = null };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Postcode);
        }

        [Test]
        public void AddressDTO_PostcodeIsEmpty_ShouldHaveAnError()
        {
            var model = new AddressDTO { Postcode = string.Empty };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Postcode);
        }

        [Test]
        public void AddressDTO_PostcodeIsTooLong_ShouldHaveAnError()
        {
            var model = new AddressDTO { Postcode = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjkfewfwefwefewfwfefwfewfewfewefwscasdqwdnfdjkndfjnkdfkjndfnjkfd" };

            var result = _addressValidator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Postcode);
        }
    }
}
