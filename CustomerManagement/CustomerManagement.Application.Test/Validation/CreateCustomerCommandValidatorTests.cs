﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation.TestHelper;
using CustomerManagement.Application.Customers.Create;
using CustomerManagement.Application.Commands.Create;

namespace CustomerManagement.Application.Test.Validation
{
    [TestFixture]
    public class CreateCustomerCommandValidatorTests
    {
        private CreateCustomerCommandValidator _validator;
        private AddressDTOValidator _addressValidator;

        [SetUp]
        public void SetUp()
        {
            _addressValidator = new AddressDTOValidator();
            _validator = new CreateCustomerCommandValidator(_addressValidator);
        }

        [Test]
        public void CreateCustomerCommand_TitleIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Title = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Title);
        }

        [Test]
        public void CreateCustomerCommand_TitleIsEmpty_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Title = string.Empty };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Title);
        }

        [Test]
        public void CreateCustomerCommand_TitleIsTooLong_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Title = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjknfdjkndfjnkdfkjndfnjkfd" };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Title);
        }

        [Test]
        public void CreateCustomerCommand_ForenameIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Title = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Forename);
        }

        [Test]
        public void CreateCustomerCommand_ForenameIsEmpty_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Title = string.Empty };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Forename);
        }

        [Test]
        public void CreateCustomerCommand_ForenameIsTooLong_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Forename = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjknfdjkndfjnkdfkjndfnjkfd" };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Forename);
        }

        [Test]
        public void CreateCustomerCommand_SurnameIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Surname = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Surname);
        }

        [Test]
        public void CreateCustomerCommand_SurnameIsEmpty_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Surname = string.Empty };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Surname);
        }

        [Test]
        public void CreateCustomerCommand_SurnameIsTooLong_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { Surname = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjknfdjkndfjnkdfkjndfnjkfd" };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.Surname);
        }

        [Test]
        public void CreateCustomerCommand_EmailAddressIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { EmailAddress = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.EmailAddress);
        }

        [Test]
        public void CreateCustomerCommand_EmailAddressIsEmpty_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { EmailAddress = string.Empty };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.EmailAddress);
        }

        [Test]
        public void CreateCustomerCommand_EmailAddressIsNotFormattedCorrectly_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { EmailAddress = "testattest.co.uk" };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.EmailAddress);
        }

        [Test]
        public void CreateCustomerCommand_MobileNoIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { MobileNo = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.MobileNo);
        }

        [Test]
        public void CreateCustomerCommand_MobileNoIsEmpty_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { MobileNo = string.Empty };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.MobileNo);
        }

        [Test]
        public void CreateCustomerCommand_MobileNoIsTooLong_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { MobileNo = "Tjfsdjjdsjdsjsdfjndjnkdnjkdfjndfjnkdfsjknfdjkndfjnkdfkjndfnjkfd" };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.MobileNo);
        }

        [Test]
        public void CreateCustomerCommand_MainAddressIsNull_ShouldHaveAnError()
        {
            var model = new CreateCustomerCommand { MainAddress = null };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(customer => customer.MainAddress);
        }

    }
}
