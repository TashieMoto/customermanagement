﻿using AutoMapper;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Application.Queries.GetAll;
using CustomerManagement.Application.Queries.GetCustomer;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test.Queries
{
    [TestFixture]
    public class GetCustomerDetailsQueryHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IMapper> _imapper;
        private GetCustomerDetailsQueryHandler _getCustomerDetailsQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _imapper = new Mock<IMapper>();

            _getCustomerDetailsQueryHandler = new GetCustomerDetailsQueryHandler(_unitOfWork.Object, _imapper.Object);
        }

        [Test]
        public void Handle_CustomerDoesNotExist_NotFoundExceptionIsThrown()
        {
            var query = new GetCustomerDetailsQuery
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.GetCustomerDetails(query.Id)).ReturnsAsync((Customer)null);

            Assert.ThrowsAsync<NotFoundException>(() => _getCustomerDetailsQueryHandler.Handle(query, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task Handle_CustomerIFound_CustomerIsMappedAndReturnedAsync()
        {
            var query = new GetCustomerDetailsQuery
            {
                Id = 1
            };
            _unitOfWork.Setup(x => x.CustomerRepository.GetCustomerDetails(query.Id)).ReturnsAsync(new Customer("test", "test", "test", "test", "test"));
            _unitOfWork.Setup(x => x.Commit(default(CancellationToken))).Returns(Task.FromResult(true));

            var result = await _getCustomerDetailsQueryHandler.Handle(query, new System.Threading.CancellationToken());

            _imapper.Verify(x => x.Map<CustomerVM>(It.IsAny<Customer>()));
        }
    }
}
