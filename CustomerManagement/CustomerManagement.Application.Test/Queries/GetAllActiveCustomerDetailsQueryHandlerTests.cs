﻿using AutoMapper;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Application.Queries.GetAllActiveCustomer;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test.Queries
{
    [TestFixture]
    public class GetAllActiveCustomerDetailsQueryHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IMapper> _imapper;
        private GetAllActiveCustomerDetailsQueryHandler _getAllActiveCustomerDetailsQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _imapper = new Mock<IMapper>();

            _getAllActiveCustomerDetailsQueryHandler = new GetAllActiveCustomerDetailsQueryHandler(_unitOfWork.Object, _imapper.Object);
        }

        [Test]
        public void Handle_NoActiveCustomersExistAndIsNull_NotFoundExceptionIsThrown()
        {
            var command = new GetAllActiveCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllActiveCustomerDetails()).ReturnsAsync((IEnumerable<Customer>)null);

            Assert.ThrowsAsync<NotFoundException>(() => _getAllActiveCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public void Handle_NoActiveCustomersExistAndIsAnEmptyList_NotFoundExceptionIsThrown()
        {
            var command = new GetAllActiveCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllActiveCustomerDetails()).ReturnsAsync(new List<Customer>());

            Assert.ThrowsAsync<NotFoundException>(() => _getAllActiveCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public void Handle_ActiveCustomersExist_MappedListIsReturned()
        {
            var command = new GetAllActiveCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllActiveCustomerDetails()).ReturnsAsync(new List<Customer>() {new Customer("test", "test", "test", "test", "test") });

            var result =  _getAllActiveCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken());

            _imapper.Verify(x => x.Map<IEnumerable<CustomerVM>>(It.IsAny<IEnumerable<Customer>>()));
        }
    }
}
