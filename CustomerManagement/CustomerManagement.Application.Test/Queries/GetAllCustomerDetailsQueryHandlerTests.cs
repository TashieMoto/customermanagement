﻿using AutoMapper;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Application.Queries.GetAllCustomer;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Test.Queries
{
    [TestFixture]
    public class GetAllCustomerDetailsQueryHandlerTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private Mock<IMapper> _imapper;
        private GetAllCustomerDetailsQueryHandler _getAllCustomerDetailsQueryHandler;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _imapper = new Mock<IMapper>();

            _getAllCustomerDetailsQueryHandler = new GetAllCustomerDetailsQueryHandler(_unitOfWork.Object, _imapper.Object);
        }

        [Test]
        public void Handle_NoCustomersExistAndIsNull_NotFoundExceptionIsThrown()
        {
            var command = new GetAllCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllCustomerDetails()).ReturnsAsync((IEnumerable<Customer>)null);

            Assert.ThrowsAsync<NotFoundException>(() => _getAllCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public void Handle_NoCustomersExistAndIsAnEmptyList_NotFoundExceptionIsThrown()
        {
            var command = new GetAllCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllCustomerDetails()).ReturnsAsync(new List<Customer>());

            Assert.ThrowsAsync<NotFoundException>(() => _getAllCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken()));
        }

        [Test]
        public void Handle_ActiveCustomersExist_MappedListIsReturned()
        {
            var command = new GetAllCustomerDetailsQuery();
            _unitOfWork.Setup(x => x.CustomerRepository.GetAllCustomerDetails()).ReturnsAsync(new List<Customer>() { new Customer("test", "test", "test", "test", "test") });

            var result = _getAllCustomerDetailsQueryHandler.Handle(command, new System.Threading.CancellationToken());

            _imapper.Verify(x => x.Map<IEnumerable<CustomerVM>>(It.IsAny<IEnumerable<Customer>>()));
        }
    }
}
