﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Commands.ChangeMainAddress
{
    public class ChangeMainAddressCommand : EntityCommand<int>, IRequest<bool>
    {
        public int AddressId { get; set; }
    }
}
