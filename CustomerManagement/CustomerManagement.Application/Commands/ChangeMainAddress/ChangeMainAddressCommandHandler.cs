﻿using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Commands.ChangeMainAddress
{
    public class ChangeMainAddressCommandHandler : IRequestHandler<ChangeMainAddressCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChangeMainAddressCommandHandler(
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(ChangeMainAddressCommand request, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.GetCustomerDetails(request.Id);

            if (customer == null)
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer.Id));
            }

            var address = customer.Addresses.FirstOrDefault(x => x.Id == request.AddressId);

            if (address == null)
            {
                throw new NotFoundException(nameof(Address), nameof(Address.Id));
            }

            customer.Addresses.Select(c => { c.IsMainAddress = false; return c; }).ToList();
            address.IsMainAddress = true;

            _unitOfWork.CustomerRepository.Update(customer);

            return await _unitOfWork.Commit(cancellationToken);
        }
    }
}
