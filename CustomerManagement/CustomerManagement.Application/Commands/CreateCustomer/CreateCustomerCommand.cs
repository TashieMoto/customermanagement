﻿using MediatR;
using System.Collections.Generic;

namespace CustomerManagement.Application.Customers.Create
{
    public class CreateCustomerCommand : IRequest<bool>
    {
        public string Title { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public AddressDTO MainAddress { get; set; }
        public ICollection<AddressDTO> Addresses { get; set; }

        public class AddressDTO
        {
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string Town { get; set; }
            public string County { get; set; }
            public string Postcode { get; set; }
            public string Country { get; set; }
        }
    }
}
