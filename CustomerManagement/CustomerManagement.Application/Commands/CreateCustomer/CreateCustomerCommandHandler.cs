﻿using CustomerManagement.Application.Customers.Create;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Create
{
    public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateCustomerCommandHandler(
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.FindAsync(request.Title, request.Forename, request.Surname, request.EmailAddress, request.MobileNo);

            if (customer != null)
            {
                throw new CustomerAlreadyExistsException(customer.Forename, customer.Surname, customer.EmailAddress);
            }

            customer = new Customer(request.Title, request.Forename, request.Surname, request.EmailAddress, request.MobileNo);

            customer.AddMainAddress(request.MainAddress.AddressLine1,
                request.MainAddress.AddressLine2,
                request.MainAddress.Postcode,
                request.MainAddress.County,
                request.MainAddress.AddressLine2,
                request.MainAddress.Country
                );

            foreach(var addAddress in request.Addresses)
            {
                customer.AddAdditionalAddress(addAddress.AddressLine1,
                addAddress.AddressLine2,
                addAddress.Postcode,
                addAddress.County,
                addAddress.AddressLine2,
                addAddress.Country);
            }

            await _unitOfWork.CustomerRepository.Add(customer);

            return await _unitOfWork.Commit(cancellationToken);
        }
    }

}
