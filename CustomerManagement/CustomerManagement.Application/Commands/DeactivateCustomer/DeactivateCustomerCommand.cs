﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Commands.DeactivateCustomer
{
    public class DeactivateCustomerCommand : EntityCommand<int>, IRequest<bool>
    {
    }
}
