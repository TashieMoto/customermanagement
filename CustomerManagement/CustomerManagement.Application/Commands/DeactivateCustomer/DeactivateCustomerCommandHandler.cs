﻿using CustomerManagement.Application.Commands.Delete;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Commands.DeactivateCustomer
{
    public class DeactivateCustomerCommandHandler : IRequestHandler<DeactivateCustomerCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeactivateCustomerCommandHandler(
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(DeactivateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.Get(request.Id);

            if (customer == null)
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer.Id));
            }

            customer.IsActive = false;

            _unitOfWork.CustomerRepository.Update(customer);

            return await _unitOfWork.Commit(cancellationToken);
        }
    }
}
