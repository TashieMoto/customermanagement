﻿using MediatR;

namespace CustomerManagement.Application.Commands.Delete
{
    public class DeleteCustomerCommand : EntityCommand<int>, IRequest<bool>
    {    
    }
}
