﻿using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Commands.Delete
{
    public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCustomerCommandHandler(
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.Get(request.Id);

            if (customer == null)
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer.Id));
            }

            _unitOfWork.CustomerRepository.Delete(customer);

            return await _unitOfWork.Commit(cancellationToken);
        }
    }
}
