﻿using CustomerManagement.Application.Exceptions;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Commands.DeleteAddress
{
    public class DeleteAddressCommandHandler : IRequestHandler<DeleteAddressCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteAddressCommandHandler(
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(DeleteAddressCommand request, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.GetCustomerDetails(request.Id);

            if (customer == null)
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer.Id));
            }

            var address = customer.Addresses.FirstOrDefault(x => x.Id == request.AddressId);

            if (address == null)
            {
                throw new NotFoundException(nameof(Address), nameof(Address.Id));
            }

            if (address.IsMainAddress)
            {
                throw new InvalidAddressRemovalException(request.AddressId);
            }

            _unitOfWork.CustomerRepository.RemoveAddress(address);

            return await _unitOfWork.Commit(cancellationToken);
        }
    }
}
