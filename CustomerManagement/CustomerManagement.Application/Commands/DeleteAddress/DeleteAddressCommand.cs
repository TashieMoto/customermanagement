﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Commands.DeleteAddress
{
    public class DeleteAddressCommand : EntityCommand<int>, IRequest<bool>
    {
        public int AddressId { get; set; }
    }
}
