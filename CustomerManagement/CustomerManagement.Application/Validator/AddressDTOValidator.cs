﻿using FluentValidation;
using static CustomerManagement.Application.Customers.Create.CreateCustomerCommand;

namespace CustomerManagement.Application.Commands.Create
{
    public class AddressDTOValidator : AbstractValidator<AddressDTO>
    {
        public AddressDTOValidator()
        {
            RuleFor(a => a.AddressLine1)
                .MaximumLength(80)
                .NotEmpty();

            RuleFor(a => a.AddressLine2)
                .MaximumLength(80);

            RuleFor(a => a.Town)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(a => a.County)
                 .MaximumLength(50);

            RuleFor(a => a.Country)
                 .MaximumLength(50);

            RuleFor(a => a.Postcode)
             .MaximumLength(10)
             .NotEmpty();
        }
    }
}
