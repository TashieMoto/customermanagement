﻿using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using FluentValidation;
using static CustomerManagement.Application.Customers.Create.CreateCustomerCommand;

namespace CustomerManagement.Application.Customers.Create
{
    public class CreateCustomerCommandValidator : AbstractValidator<CreateCustomerCommand>
    {
        public CreateCustomerCommandValidator(IValidator<AddressDTO> addressValidator)
        {
            RuleFor(a => a.Forename)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(a => a.Surname)
                .MaximumLength(50)
                .NotEmpty();

            RuleFor(a => a.Title)
                .MaximumLength(20)
                .NotEmpty();

            RuleFor(a => a.EmailAddress)
                .MaximumLength(75)
                .EmailAddress(FluentValidation.Validators.EmailValidationMode.AspNetCoreCompatible)
                .NotEmpty();

            RuleFor(a => a.MobileNo)
                .MaximumLength(15)
                .NotEmpty();

            RuleFor(a => a.MainAddress)
                .SetValidator(addressValidator)
                .NotEmpty();

            RuleForEach(a => a.Addresses)
                .SetValidator(addressValidator);
        }
    }
}
