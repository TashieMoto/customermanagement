﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Exceptions
{
    public class CustomerAlreadyExistsException : Exception
    {
        public CustomerAlreadyExistsException(string forname, string surname, string emailAddress) 
            : base($"customer {forname} {surname} with email {emailAddress} exists.")
        {
        }
    }
}
