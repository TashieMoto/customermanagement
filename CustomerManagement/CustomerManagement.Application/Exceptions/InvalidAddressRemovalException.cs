﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Exceptions
{
    public class InvalidAddressRemovalException : Exception
    {
        public InvalidAddressRemovalException(int key) : base($"Address ({key}) is the main address.")
        {
        }
    }
}
