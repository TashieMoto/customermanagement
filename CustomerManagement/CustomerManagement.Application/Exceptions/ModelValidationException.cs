﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerManagement.Application.Exceptions
{
    public class ModelValidationException : Exception
    {
        public IDictionary<string, string[]> Failures { get; }
        public ModelValidationException() : base()
                => Failures = new Dictionary<string, string[]>();

        public ModelValidationException(List<ValidationFailure> failures)
            : this()
        {
            var failureGroups = failures
                .GroupBy(e => e.PropertyName, e => e.ErrorMessage);

            foreach (var failureGroup in failureGroups)
            {
                var propertyName = failureGroup.Key;
                var propertyFailures = failureGroup.ToArray();

                Failures.Add(propertyName, propertyFailures);
            }
        }
    }

}
