﻿using AutoMapper;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomerManagement.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Address, AddressVM>();
            CreateMap<Customer, CustomerVM>()
                .ForMember(x => x.MainAddress, m => m.MapFrom(vm => vm.Addresses.First(x => x.IsMainAddress)))
                .ForMember(x => x.AdditionalAddresses, m => m.MapFrom(vm => vm.Addresses.Where(x => !x.IsMainAddress)));
        }
    }
}
