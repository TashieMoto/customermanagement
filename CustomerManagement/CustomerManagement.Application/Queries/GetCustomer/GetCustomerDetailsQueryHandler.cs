﻿using AutoMapper;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Application.Queries.GetAll;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Queries.GetCustomer
{
    public class GetCustomerDetailsQueryHandler : IRequestHandler<GetCustomerDetailsQuery, CustomerVM>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCustomerDetailsQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CustomerVM> Handle(
            GetCustomerDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.CustomerRepository.GetCustomerDetails(request.Id);

            if (customer == null)
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer.Id));
            }

            return _mapper.Map<CustomerVM>(customer);
        }
    }
}
