﻿using CustomerManagement.Application.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Queries.GetAll
{
    public class GetCustomerDetailsQuery : IRequest<CustomerVM>
    {
        public int Id { get; set; }
    }
}
