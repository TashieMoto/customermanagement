﻿using AutoMapper;
using CustomerManagement.Application.Exceptions;
using CustomerManagement.Application.ViewModels;
using CustomerManagement.Domain;
using CustomerManagement.Domain.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Application.Queries.GetAllActiveCustomer
{
    public class GetAllActiveCustomerDetailsQueryHandler : IRequestHandler<GetAllActiveCustomerDetailsQuery, IEnumerable<CustomerVM>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllActiveCustomerDetailsQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CustomerVM>> Handle(
            GetAllActiveCustomerDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var customers = await _unitOfWork.CustomerRepository.GetAllActiveCustomerDetails();

            if (customers == null || !customers.Any())
            {
                throw new NotFoundException(nameof(Customer), nameof(Customer));
            }

            return _mapper.Map<IEnumerable<CustomerVM>>(customers);
        }
    }
}
