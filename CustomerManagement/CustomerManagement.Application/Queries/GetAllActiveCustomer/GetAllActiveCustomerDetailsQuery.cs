﻿using CustomerManagement.Application.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Queries.GetAllActiveCustomer
{
    public class GetAllActiveCustomerDetailsQuery : IRequest<IEnumerable<CustomerVM>>
    {
    }
}
