﻿using CustomerManagement.Application.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.Queries.GetAllCustomer
{
    public class GetAllCustomerDetailsQuery : IRequest<IEnumerable<CustomerVM>>
    {
    }
}
