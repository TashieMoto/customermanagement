﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerManagement.Application.ViewModels
{
    public class CustomerVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public AddressVM MainAddress { get; set; }
        public List<AddressVM> AdditionalAddresses { get; set; }
    }

    public class AddressVM
    {
        public int Id { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
    }
}
