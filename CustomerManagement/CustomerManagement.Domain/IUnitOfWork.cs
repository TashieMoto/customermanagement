﻿using CustomerManagement.Domain.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerManagement.Domain
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepository CustomerRepository { get; }
        Task<bool> Commit(CancellationToken token);
    }
}
