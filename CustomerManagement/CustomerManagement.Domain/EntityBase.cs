﻿namespace CustomerManagement.Domain
{
    public abstract class EntityBase<T> where T : struct
    {
        int? _requestedHashCode;

        public virtual T Id { get; private set; } = default;

        public bool IsTransient() => Id.Equals(default(T));

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is EntityBase<T>))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (GetType() != obj.GetType())
            {
                return false;
            }

            EntityBase<T> item = (EntityBase<T>)obj;

            if (item.IsTransient() || IsTransient())
            {
                return false;
            }
            
            return item.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                {
                    _requestedHashCode = Id.GetHashCode() ^ 31;
                }

                return _requestedHashCode.Value;
            }
            
            return base.GetHashCode();
        }
    }
}
