﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerManagement.Domain.Models
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Task<Customer> GetCustomerDetails(int id);
        void RemoveAddress(Address address);
        Task<IEnumerable<Customer>> GetAllCustomerDetails();
        Task<IEnumerable<Customer>> GetAllActiveCustomerDetails();
        Task<Customer> FindAsync(string title, string forename, string surname, string emailAddress, string mobileNo);
    }
}
