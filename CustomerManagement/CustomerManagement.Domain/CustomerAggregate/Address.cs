﻿using System.Collections.Generic;

namespace CustomerManagement.Domain.Models
{
    public class Address : EntityBase<int>
    {
        public Address(string addressLine1, string addressLine2, string town, string county, string postcode, string country, bool isMainAddress)
        {
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Town = town;
            County = county;
            Postcode = postcode;
            Country = country;
            IsMainAddress = isMainAddress;
        }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public bool IsMainAddress { get; set; } = false;
    }
}
