﻿using System.Collections.Generic;

namespace CustomerManagement.Domain.Models
{
    public class Customer : EntityBase<int>, IAggregateRoot
    {
        public Customer (string title, string forename, string surname, string emailAddress, string mobileNo)
        {
            Title = title;
            Forename = forename;
            Surname = surname;
            EmailAddress = emailAddress;
            MobileNo = mobileNo;
        }

        public string Title { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Address> Addresses { get; set; } = new HashSet<Address>();

        public void AddMainAddress(string addressLine1, string town, string postcode, string county, string addressLine2, string country)
        {
            Addresses.Add(new Address(addressLine1, addressLine2, town, county, postcode, country, true));
        }

        public void AddAdditionalAddress(string addressLine1, string town, string postcode, string county, string addressLine2, string country)
        {
            Addresses.Add(new Address(addressLine1, addressLine2, town, county, postcode, country, false));
        }
    }
}
