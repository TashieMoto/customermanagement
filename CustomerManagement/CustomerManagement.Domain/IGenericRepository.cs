﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CustomerManagement.Domain
{
    public interface IGenericRepository<T> where T : class, IAggregateRoot
    {
        Task<T> Get(int id);
        Task<IEnumerable<T>> GetAll();
        Task Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
