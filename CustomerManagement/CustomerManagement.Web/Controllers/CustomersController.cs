﻿using CustomerManagement.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CustomerManagement.Application.Customers.Create;
using CustomerManagement.Application.Commands.Delete;
using CustomerManagement.Application.Queries.GetAll;
using CustomerManagement.Application.Queries.GetAllCustomer;
using CustomerManagement.Application.Commands.DeactivateCustomer;
using CustomerManagement.Application.Queries.GetAllActiveCustomer;
using CustomerManagement.Application.Commands.DeleteAddress;
using CustomerManagement.Application.Commands.ChangeMainAddress;

namespace Customer.Web.Controllers
{
    public class CustomersController : ApiController
    {
        public CustomersController(){}

        [HttpPost]
        public async Task<ActionResult> Create(CreateCustomerCommand command) 
        { 
            var commandResult = await Mediator.Send(command);

            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete]
        [Route("{Id}")]
        public async Task<ActionResult> Delete([FromRoute] DeleteCustomerCommand command)
        {
            var commandResult = await Mediator.Send(command);

            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var queryResult = await Mediator.Send(new GetAllCustomerDetailsQuery());
            return Ok(queryResult);
        }

        [HttpGet]
        [Route("Active")]
        public async Task<ActionResult> GetAllActive()
        {
            var queryResult = await Mediator.Send(new GetAllActiveCustomerDetailsQuery());
            return Ok(queryResult);
        }


        [HttpGet]
        [Route("{Id}")]
        public async Task<ActionResult> Get([FromRoute] GetCustomerDetailsQuery query)
        {
            var queryResult = await Mediator.Send(query);
            return Ok(queryResult);
        }

        [HttpPut]
        [Route("{Id}/Deactivate")]
        public async Task<ActionResult> Deactivate([FromRoute] DeactivateCustomerCommand command)
        {
            var commandResult = await Mediator.Send(command);

            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete]
        [Route("{Id}/Addresses/{AddressId}")]
        public async Task<ActionResult> DeleteAddress([FromRoute] DeleteAddressCommand command)
        {
            var commandResult = await Mediator.Send(command);

            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPut]
        [Route("{Id}/Addresses/{AddressId}/SetAsMainAddress")]
        public async Task<ActionResult> SetAsMainAddress([FromRoute] ChangeMainAddressCommand command)
        {
            var commandResult = await Mediator.Send(command);

            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
